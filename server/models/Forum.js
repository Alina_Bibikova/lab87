const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ForumSchema = new Schema({
    title: {
        type: String,
    },
    description: {
        type: String,
        required: true,
    },
    datetime: {
        type: String,
        unique: true
    },
    image: String
});

ForumSchema.methods.generateDateTime = function () {
    this.datetime  = new Date().toString('yyyy-MM-dd')
};

const Forum = mongoose.model('Forum', ForumSchema);

module.exports = Forum;