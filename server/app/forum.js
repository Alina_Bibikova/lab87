const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const Forum = require('../models/Forum');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', (req, res) => {
    Forum.find().sort('-datetime')
        .then(result => res.send(result))
        .catch(() => res.sendStatus(500));
});

router.get('/:id', (req, res) => {
    Forum.findById(req.params.id)
        .then(result => {
            if (result) return res.send(result);
            res.sendStatus(404);
        })
        .catch(() => res.sendStatus(500));
});

router.post('/', upload.single('image'), (req, res) => {
    const ForumData = req.body;
    if (req.file) {
        ForumData.image = req.file.filename;
    }
    const forum = new Forum(ForumData);

    forum.generateDateTime();

    forum.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error))
});

module.exports = router;