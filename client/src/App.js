import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {Route, Switch, withRouter} from "react-router-dom";

import Toolbar from "./components/UI/Toolbar/Toolbar";
import Forum from "./containers/Forum/Forum";
// import NewProduct from "./containers/NewProduct/NewProduct";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {connect} from "react-redux";
import {logoutUser} from "./store/actions/usersActions";
import NewPost from "./containers/NewPost/NewPost";
import InfoForum from "./containers/InfoForum/InfoForum";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <Toolbar user={this.props.user}
                   logout={this.props.logoutUser}/>
        </header>
        <Container style={{marginTop: '20px'}}>
          <Switch>
            <Route path="/" exact component={Forum} />
            <Route path="/post/new" exact component={NewPost} />
            <Route path="/register" exact component={Register} />
            <Route path="/login" exact component={Login} />
            <Route path="/forum/:id" exact component={InfoForum}/>
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    logoutUser: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
