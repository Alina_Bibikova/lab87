import {FETCH_DATA_FAILURE,
    FETCH_FORUM_SUCCESS,
    FETCH_FORUMID_SUCCESS}
    from "../actions/forumActions";

const initialState = {
  forum: [],
  forumId: [],
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_FORUM_SUCCESS:
      return {
        ...state,
          forum: action.forum,
          error: null
      };

      case FETCH_FORUMID_SUCCESS:
          return {
            ...state,
              forumId: action.forumId,
              error: null
          };

      case FETCH_DATA_FAILURE:
        return {
            ...state,
            error: action.error
        };

    default:
      return state;
  }
};

export default reducer;
