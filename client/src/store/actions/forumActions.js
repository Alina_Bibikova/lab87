import axios from '../../axios-api';

export const FETCH_DATA_REQUEST = 'FETCH_DATA_REQUEST';
export const FETCH_DATA_FAILURE = 'FETCH_DATA_FAILURE';

export const FETCH_FORUM_SUCCESS = 'FETCH_FORUM_SUCCESS';
export const CREATE_FORUM_SUCCESS = 'CREATE_FORUM_SUCCESS';
export const FETCH_FORUMID_SUCCESS = 'FETCH_FORUMID_SUCCESS';

export const fetchForumSuccess = forum => ({type: FETCH_FORUM_SUCCESS, forum});
export const createForumSuccess = () => ({type: CREATE_FORUM_SUCCESS});
export const fetchDataRequest = () => ({type: FETCH_DATA_REQUEST});
export const fetchDataFailure = error => ({type: FETCH_DATA_FAILURE, error});
export const fetchForumIdSuccess = forumId => ({type: FETCH_FORUMID_SUCCESS, forumId});

export const fetchForum = () => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get('/forum');
            dispatch(fetchForumSuccess(response.data))
        } catch (e) {
            dispatch(fetchDataFailure(e));
        }
    }
};

export const createForum = forumData => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.post('/forum', forumData);
            dispatch(createForumSuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }

    };
};

export const fetchForumId = id => {
    return async dispatch => {
        dispatch(fetchDataRequest());

        try {
            const response = await axios.get(`/forum/${id}`);
            dispatch(fetchForumIdSuccess(response.data));
        } catch (error) {
            dispatch(fetchDataFailure(error));
        }
    }
};
