import React, {Component} from 'react';
import {fetchForumId} from "../../store/actions/forumActions";
import {connect} from "react-redux";
import InfoFormListItem from "../../components/InfoFormListItem/InfoFormListItem";

class InfoForum extends Component {
    componentDidMount() {
        this.props.onFetchForumId(this.props.match.params.id)
    }
    render() {
        return (
            this.props.forumId ? <div>
                <h2>Info post</h2>
                    <InfoFormListItem
                        image={this.props.forumId.image}
                        description={this.props.forumId.description}
                        title={this.props.forumId.title}
                        datetime={this.props.forumId.datetime}
                    />
            </div> : null
        );
    }
}

const mapStateToProps = state => ({
    forumId: state.forum.forumId
});

const mapDispatchToProps = dispatch => ({
    onFetchForumId: id => dispatch(fetchForumId(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(InfoForum);

