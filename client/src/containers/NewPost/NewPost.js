import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {createForum} from "../../store/actions/forumActions";
import PostForum from "../../components/PostForum/PostForum";

class NewPost extends Component {
    createPost = forumData => {
        this.props.onForumCreated(forumData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New post</h2>
                <PostForum
                    onSubmit={this.createPost}
                />
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onForumCreated: forumData => dispatch(createForum(forumData)),
});

export default connect(null, mapDispatchToProps)(NewPost);