import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchForum} from "../../store/actions/forumActions";
import ForumListItem from "../../components/ForumListItem/ForumListItem";

class Forum extends Component {
    componentDidMount() {
        this.props.onFetchForum();
    }

    render() {
        return (
            <Fragment>
                {this.props.forum.map(forum => (
                    <ForumListItem
                        key={forum._id}
                        _id={forum._id}
                        title={forum.title}
                        datetime={forum.datetime}
                        image={forum.image}
                    />
                ))}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    forum: state.forum.forum
});

const mapDispatchToProps = dispatch => ({
    onFetchForum: () => dispatch(fetchForum())
});

export default connect(mapStateToProps, mapDispatchToProps)(Forum);