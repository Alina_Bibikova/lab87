import React from 'react';
import ForumThumbnail from "../ForumThumbnail/ForumThumbnail";
import {Card, CardBody, CardTitle} from "reactstrap";
import {Link} from "react-router-dom";
import PropTypes from 'prop-types';

const ForumListItem = props => {
    return (
        <Card style={{marginBottom: '10px'}}>
            <CardBody>
                <ForumThumbnail image={props.image}/>
                <strong style={{marginLeft: '10px'}}>
                    {props.datetime}
                </strong>
                <CardTitle style={{marginLeft: '125px'}}>
                     <Link to={'/forum/' + props._id}>
                    {props.title}
                </Link>
                </CardTitle>
            </CardBody>
        </Card>
    );
};

ForumListItem.propTypes = {
    image: PropTypes.string,
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
};

export default ForumListItem;