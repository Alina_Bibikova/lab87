import React from 'react';
import PropTypes from "prop-types";
import ForumThumbnail from "../ForumThumbnail/ForumThumbnail";
import {Card, CardBody, CardText} from "reactstrap";

const InfoFormListItem = props => {
    return (
        <Card style={{marginTop: '10px'}}>
            <CardBody>
                <ForumThumbnail image={props.image}/>
                <CardText>
                    {props.datetime}
                </CardText>
                <CardText>
                    {props.title}
                </CardText>
                <CardText>
                    {props.description}
                </CardText>
            </CardBody>
        </Card>
    );
};

InfoFormListItem.propTypes = {
    image: PropTypes.string,
    title: PropTypes.string,
};

export default InfoFormListItem;